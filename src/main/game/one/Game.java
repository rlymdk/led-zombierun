package main.game.one;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;

@SuppressWarnings("serial")
public class Game extends JPanel {

	private static final int INCREMENT = 3, SCREENWIDTH = 720, SCREENHEIGHT = 720;
	//private static final double PLAYERINC = 5;
	private BufferedImage backgroundImage, boardImage;
	private int dy1, dy, px, py, score, time;
	private int posX[] = { 0, SCREENWIDTH / 2 + 20 };
	private int posBrainX[] = {100, 550};
	private ArrayList<Board> boards = new ArrayList<Board>();
	private ArrayList<Brain> brains = new ArrayList<Brain>();
	static final Random random = new Random();
	private BufferedImage[] player = { SpriteSheet.getSprite(0, 0), SpriteSheet.getSprite(3, 1) };
	private Animation playerAni = new Animation(player, 11);
	private Timer timer, boardT, brainsT, countdown, t;
	private boolean keyLeft, keyRight;
	private ImageIcon brainI;
	private JLabel scoreLabel, countLabel;
	
	private Rect objectBoundingRectangle;
	private int SENSITIVITY_VALUE = 20;
	private int BLUR_SIZE = 9;
	private Mat BGFrame, currentFrame, grayBG, grayImage2, differenceImage, thresholdImage;
	private VideoCapture camera;
	
	public Game() {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		init();
		add(scoreLabel);
		add(countLabel);
		playerAni.start();
		
		camera = new VideoCapture(0);
		BGFrame = new Mat();
		currentFrame = new Mat();
		grayBG = new Mat();
		grayImage2 = new Mat();
		differenceImage = new Mat();
		thresholdImage = new Mat();
		
		camera.read(BGFrame);
		Imgproc.resize(BGFrame, BGFrame, new Size(WIDTH, HEIGHT));
		Imgproc.cvtColor(BGFrame, grayBG, Imgproc.COLOR_BGR2GRAY);
		
		t = new Timer(150, new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (camera.read(currentFrame)) {
					Imgproc.resize(currentFrame, currentFrame, new Size(WIDTH, HEIGHT));
					Imgproc.cvtColor(currentFrame, grayImage2, Imgproc.COLOR_BGR2GRAY);

				} else{
					System.out.println("WTF!");
				}

				// DIFFERENCE
				Core.absdiff(grayBG, grayImage2, differenceImage);
				Imgproc.threshold(differenceImage, thresholdImage, SENSITIVITY_VALUE, 255, Imgproc.THRESH_BINARY);
				Imgproc.GaussianBlur(thresholdImage, thresholdImage, new Size(BLUR_SIZE, BLUR_SIZE), 0);
				Imgproc.threshold(thresholdImage, thresholdImage, SENSITIVITY_VALUE, 255, Imgproc.THRESH_BINARY);

				searchForMovement(thresholdImage, currentFrame);
			}
		}); t.start();
		
		countdown = new Timer(1000, new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				System.out.println("1");
				--time;
				countLabel.setText("TIME: "+time);
				if(time<5)  countLabel.setForeground(Color.RED);
			}
		}); countdown.start();
		

		boards.add(new Board(posX[random.nextInt(2)], -20, SCREENWIDTH / 2 - 20));
		boardT = new Timer(2350, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				boards.add(new Board(posX[random.nextInt(2)], -20, SCREENWIDTH / 2 - 20));
			}
		}); boardT.start();
		
		brains.add(new Brain(posBrainX[random.nextInt(2)], -60));
		brainsT = new Timer(470, new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				brains.add(new Brain(posBrainX[random.nextInt(2)], -60));
			}
		}); brainsT.start();
		
		timer = new Timer(1000 / 60, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				playerAni.update();
				moveBackground();
				moveBoards();
				moveBrains();
				repaint();
				
				if(keyLeft) px = 60;
				if(keyRight) px = 530;
//				if(px < 0) px = -5;
//				if(px > SCREENWIDTH-123) px = SCREENWIDTH-120; 
				
//				if (checkCollision()) timer.stop();
				if (checkBrains()) {
					scoreLabel.setText("SCORE: " + Integer.toString(score));
					System.out.println(score++);
				}
			}
		}); timer.start();
	}

	protected void init() {
		setLayout(null);
		setFocusable(true);
		addKeyListener(new MyKeyHandler());
		initImages();
		dy1 = 0;
		dy = 0;
		px = 60;
		time = 30;
		score = 0;
		py = 580;
		keyLeft = false;
		keyRight = false;
		
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		ge.getAllFonts();
		Font font = new Font("Jokerman", Font.PLAIN, 20);
		scoreLabel = new JLabel("SCORE: 0");
		scoreLabel.setBounds(10, 0, 150, 50);
		scoreLabel.setFont(font);
		scoreLabel.setForeground(Color.MAGENTA);
		countLabel = new JLabel("TIME: 30");
		countLabel.setBounds(390, 0, 150, 50);
		countLabel.setFont(font);
		countLabel.setForeground(Color.GREEN);
	}
	
	public void endGame(){
		camera.release();
		timer.stop();
		brainsT.stop();
		boardT.stop();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, getWidth(), getHeight());
		g.drawImage(backgroundImage, 0, dy - SCREENHEIGHT, SCREENWIDTH, SCREENHEIGHT, null);
		g.drawImage(backgroundImage, 0, dy1, SCREENWIDTH, SCREENHEIGHT, null);
		for (Board board : boards) {
			g.drawImage(boardImage, board.getX(), board.getY(), board.getW(), board.getH(), null);
		}
		for (Brain brain : brains) {
			brainI.paintIcon(null, g, brain.getX(), brain.getY());
			//g.drawRect(brain.getX(), brain.getY(), brain.getW(), brain.getH());
		}
		g.drawImage(playerAni.getSprite(), px, py, null);
		//g.drawRect(px + 10, py + 10, 100, 90);
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(SCREENWIDTH, SCREENHEIGHT);
	}

	private void initImages() {
		try {
			backgroundImage = ImageIO.read(new File("images/grass_template.jpg"));
			boardImage = ImageIO.read(new File("images/board.png"));
			brainI = new ImageIcon("images/brain.gif");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void moveBackground() {
		if ((dy - SCREENHEIGHT) % SCREENHEIGHT == 0)
			dy = 0;
		if ((dy1 - SCREENHEIGHT * 2) % SCREENHEIGHT == 0)
			dy1 = 0;
		dy1 += INCREMENT;
		dy += INCREMENT;
	}

	private void moveBoards() {
		Iterator<Board> it = boards.iterator();
		while (it.hasNext()) {
			Board board = it.next();
			if (board.getY() > SCREENHEIGHT) {
				it.remove();
			} else {
				int y = board.getY() + INCREMENT;
				board.setY(y);
			}
		}
	}
	
	private void moveBrains() {
		Iterator<Brain> it = brains.iterator();
		while (it.hasNext()) {
			Brain brain= it.next();
			if (brain.getY() > SCREENHEIGHT) {
				it.remove();
			} else {
				int y = brain.getY() + INCREMENT;
				brain.setY(y);
			}
		}
	}

	private boolean checkCollision() {
		for (Board board : boards) {
			Rectangle b = new Rectangle(board.getX(), board.getY(), board.getW(), board.getH());
			Rectangle p = new Rectangle(px + 10, py + 10, 100, 90);
			if (b.intersects(p) && board.getY()-95/2 < py) {
				System.out.println("instersects");
				break;
			} else
				return false;
		}
		return true;
	}
	
	private boolean checkBrains() {
		Rectangle p = new Rectangle(px + 8, py + 9, 105, 95);
		Iterator<Brain> it = brains.iterator();
		while(it.hasNext()) {
			Brain brain = it.next();
			Rectangle b = new Rectangle(brain.getX(), brain.getY(), brain.getW(), brain.getH());
			if (b.intersects(p)) {
				it.remove();
				break;
			} else
				return false;
		}
		return true;
	}

	public class MyKeyHandler extends KeyAdapter {
		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_LEFT) {
				keyLeft = true;
			}
			else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
				keyRight = true;
			}
		}

		public void keyReleased(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_LEFT) {
				keyLeft = false;
			}
			else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
				keyRight = false;
			}
		}
	}
	
	public void searchForMovement(Mat thresholdImage, Mat frame) {
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		Mat hierarchy = new Mat();
		Imgproc.findContours(thresholdImage, contours, hierarchy, 
				Imgproc.RETR_EXTERNAL, 
				Imgproc.CHAIN_APPROX_SIMPLE);

		objectBoundingRectangle = new Rect(0, 0, 0, 0);
		for (int i = 0; i < contours.size(); i++) {
			objectBoundingRectangle = Imgproc.boundingRect(contours.get(i));
			if (objectBoundingRectangle.area() > 80000.0) {
				if((objectBoundingRectangle.tl().x + objectBoundingRectangle.br().x)/2 > 512) {
					keyLeft = true; keyRight = false;
				}
				if((objectBoundingRectangle.tl().x + objectBoundingRectangle.br().x)/2 < 512) {
					keyRight = true; keyLeft = false;
				}
				Core.rectangle(frame, objectBoundingRectangle.tl(), objectBoundingRectangle.br(),
						new Scalar(0, 255, 0));
			}
		}
	}

//	public static void main(String[] args) {
//		SwingUtilities.invokeLater(new Runnable() {
//			public void run() {
//				JFrame frame = new JFrame();
//				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//				frame.add(new Game());
//				frame.pack();
//				frame.setLocationRelativeTo(null);
//				frame.setVisible(true);
//			}
//		});
//	}
}
