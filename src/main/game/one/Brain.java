package main.game.one;

public class Brain {
	
	private int width=50, height=37, x, y;
	
	public Brain(int x, int y){
		this.x = x;
		this.y = y;
	}
	
	public int getW(){
		return this.width;
	}
	
	public int getH(){
		return this.height;
	}
	
	public void setX(int x){
		this.x = x;
	}
	
	public void setY(int y){
		this.y = y;
	}
	
	public int getX(){
		return this.x;
	}
	
	public int getY(){
		return this.y;
	}
}
