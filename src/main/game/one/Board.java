package main.game.one;

public class Board{
	
	private int positionY, positionX, height=20, width;
	
	public Board(int positionX, int positionY, int width){
		this.positionX = positionX;
		this.positionY = positionY;
		this.width = width;
	}
	
	public int getX(){
		return this.positionX;
	}
	
	public int getY(){
		return this.positionY;
	}
	
	public int getH(){
		return this.height;
	}
	
	public int getW(){
		return this.width;
	}
	
	public void setY(int y){
		this.positionY = y;
	}
}
