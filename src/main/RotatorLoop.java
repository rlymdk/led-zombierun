package main;

public class RotatorLoop {
	public static int timeFlowSeconds = 0;
	private static Thread timeFlowTimer;
	public static boolean panelIsActive = false;

	public static void startTimer() {
		if (!timeFlowTimer.isAlive()) {
			timeFlowTimer.start();
		}
	}
	public static void init(final int loopTime) {
		timeFlowTimer = new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {
						System.out.println(timeFlowSeconds);
						if (panelIsActive) {
							int durationSum = 0;
							for (int i = 0; i < Config.panelCurrent + 1; i++) {
								durationSum += Config.panelDuration[i];
							}
							if (timeFlowSeconds + 1 == durationSum) {
								panelIsActive = false;
								Config.panelCurrent++;
								Config.panelCurrent %= Config.panelQueue.length;
							}
						}
						++timeFlowSeconds;
						timeFlowSeconds %= (loopTime);
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}
}
