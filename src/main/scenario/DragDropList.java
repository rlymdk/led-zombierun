package main.scenario;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragGestureRecognizer;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.DropMode;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.border.Border;

public class DragDropList extends JList {
	DefaultListModel<String> model;

	public DragDropList() {
		super(new DefaultListModel());
		model = (DefaultListModel) getModel();
		setDragEnabled(true);
		setDropMode(DropMode.INSERT);
		
		this.setCellRenderer(new MyListCellRenderer());

		setTransferHandler(new MyListDropHandler(this));

		new MyDragListener(this);
		
		ArrayList<String> values = Main.getInitValues();

		for(int i=0;i<values.size();i++){
			model.addElement(values.get(i));
		}
		
		/*start of popup*/
		final DragDropList list = this;
		
		final JPopupMenu popup = new JPopupMenu();
		JMenuItem deleteItem = new JMenuItem("�������");
		deleteItem.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String fileName = model.getElementAt(list.getSelectedIndex());
				model.removeElementAt(list.getSelectedIndex());
				if(fileName.substring(0, 6).equalsIgnoreCase("VIDEO_")){
					File file = new File("video/"+fileName.substring(6));
					file.delete();
				}
				String[] values = new String[model.getSize()];
				for (int i = 0; i < model.getSize(); i++) {
					values[i] = (String) model.getElementAt(i);
				}
				Main.generateScenario(values);
			}
		});
		
		
		popup.add(deleteItem);
		
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				super.mouseClicked(e);
				if(SwingUtilities.isRightMouseButton(e)){
					list.setSelectedIndex(list.locationToIndex(e.getPoint()));
					popup.show(list, e.getX(), e.getY());
				}
			}
		});
		/*end of popup*/
	}
}

class MyDragListener implements DragSourceListener, DragGestureListener {
	DragDropList list;

	DragSource ds = new DragSource();

	public MyDragListener(DragDropList list) {
		this.list = list;
		DragGestureRecognizer dgr = ds.createDefaultDragGestureRecognizer(list,
				DnDConstants.ACTION_MOVE, this);

	}

	public void dragGestureRecognized(DragGestureEvent dge) {
		StringSelection transferable = new StringSelection(
				Integer.toString(list.getSelectedIndex()));
		ds.startDrag(dge, DragSource.DefaultCopyDrop, transferable, this);
	}

	public void dragEnter(DragSourceDragEvent dsde) {
	}

	public void dragExit(DragSourceEvent dse) {
	}

	public void dragOver(DragSourceDragEvent dsde) {
	}

	public void dragDropEnd(DragSourceDropEvent dsde) {
		if (dsde.getDropSuccess()) {
			System.out.println("Succeeded");
		} else {
			System.out.println("Failed");
		}
	}

	public void dropActionChanged(DragSourceDragEvent dsde) {
	}
}

class MyListCellRenderer implements ListCellRenderer{

    private final JLabel jlblCell = new JLabel(" ", JLabel.LEFT);
    Border lineBorder = BorderFactory.createLineBorder(Color.GRAY, 1);
    //Border emptyBorder = BorderFactory.createEmptyBorder(10, 10, 10, 10);

    @Override
    public Component getListCellRendererComponent(JList jList, Object value, 
            int index, boolean isSelected, boolean cellHasFocus) {

        jlblCell.setOpaque(false);
        jlblCell.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 16));
        
        String text = (String)value;
        if(text.substring(0, 6).equalsIgnoreCase("VIDEO_")){
        	text = text.substring(6);
        }
        jlblCell.setText(text);

        jlblCell.setBorder(lineBorder);

        return jlblCell;
    }
}



class MyListDropHandler extends TransferHandler {
	DragDropList list;

	public MyListDropHandler(DragDropList list) {
		this.list = list;
	}

	public boolean canImport(TransferHandler.TransferSupport support) {
		if (!support.isDataFlavorSupported(DataFlavor.stringFlavor)) {
			return false;
		}
		JList.DropLocation dl = (JList.DropLocation) support.getDropLocation();
		if (dl.getIndex() == -1) {
			return false;
		} else {
			return true;
		}
	}

	public boolean importData(TransferHandler.TransferSupport support) {
		if (!canImport(support)) {
			return false;
		}

		Transferable transferable = support.getTransferable();
		String indexString;
		try {
			indexString = (String) transferable
					.getTransferData(DataFlavor.stringFlavor);
		} catch (Exception e) {
			return false;
		}

		int dragIndex = Integer.parseInt(indexString);
		JList.DropLocation dl = (JList.DropLocation) support.getDropLocation();
		int dropTargetIndex = dl.getIndex() - 1;
		if (dropTargetIndex < 0)
			dropTargetIndex = 0;

		/* here needed code */
		DefaultListModel<String> model = (DefaultListModel) list.getModel();

		//����������� ������ ��� ���� � �����
		if(dropTargetIndex<=dragIndex){
			int minI = dropTargetIndex;
			int maxI = dragIndex;
			
			String temp = model.elementAt(minI);
			model.setElementAt(model.elementAt(maxI), minI);
			
			for(int i=minI+1;i<=maxI;i++){
				String some = model.elementAt(i);
				model.setElementAt(temp, i);
				temp = some;
			}
		}else{
			int minI = dragIndex;
			int maxI = dropTargetIndex;
			
			String temp = model.elementAt(maxI);
			model.setElementAt(model.elementAt(minI), maxI);
			for(int i=maxI-1;i>=minI;i--){
				String some = model.elementAt(i);
				model.setElementAt(temp, i);
				temp = some;
			}
		}
		//����� ����������� ������

		String[] values = new String[model.getSize()];
		for (int i = 0; i < model.getSize(); i++) {
			values[i] = (String) model.getElementAt(i);
		}
		Main.generateScenario(values);

		/* end of here needed code */

		return true;
	}
}