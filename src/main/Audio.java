package main;

import java.applet.Applet;
import java.applet.AudioClip;
import java.io.File;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.Line;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Port;

public class Audio {

	public static AudioClip racket;
	public static AudioClip wall;
	public static AudioClip goal;
	public static AudioClip intro;
	public static AudioClip outro;
	public static AudioClip step;
	public static AudioClip step_long;
	public static AudioClip missed;
	public static AudioClip instawall;

	public static void init() {
		try {
			instawall = Applet.newAudioClip(new File("sound/instawall.wav")
					.toURI().toURL());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	public static void updateVolume() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("HH");
		int hour = Integer.parseInt(sdf.format(cal.getTime()));
		if (hour >= 22 || hour < 9) {
			setVolume(0);
		} else {
			setVolume(1);
		}
	}

	public static void stopAll() {
		instawall.stop();
	}

	public static void setVolume(float value) {
		try {
			Line line = AudioSystem.getLine(Port.Info.SPEAKER);
			line.open();
			FloatControl control = (FloatControl) line
					.getControl(FloatControl.Type.VOLUME);
			control.setValue(value);
			line.close();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
	}

}
